FROM golang:latest

# Enable go modules for downloading dependencies
ENV GO111MODULE=on

RUN apt-get update
RUN apt-get install tree

#COPY ./example/mainWeb.go /go/src/gowebservice/example/
COPY ./example/3-restapiwebserver/mainrestapi.go /go/src/gowebservice/example/
COPY ./*.go /go/src/gowebservice/
COPY ./go.mod /go/src/gowebservice/
#RUN tree


#RUN go install gitlab.com/Agarnier22/gowebservice/example@latest

#RUN cd /build && git clone https://gitlab.com/Agarnier22/gowebservice.git
# Build the application
#RUN cd /go/src/gowebservice/example && go build -o /go/bin/main mainWeb.go
RUN cd /go/src/gowebservice/example && go build -o /go/bin/main mainrestapi.go


RUN cd /go && tree
# Expose listening port for application
EXPOSE 8080
#RUN [“chmod”, “+x”, "/go/src/gowebservice/example/mainWeb.go"]
CMD ["/go/bin/main"]